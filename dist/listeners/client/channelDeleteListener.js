"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
class ChannelDeleteListener extends discord_akairo_1.Listener {
    constructor() {
        super("channelDelete", {
            event: "channelDelete",
            emitter: "client",
            category: "client"
        });
    }
    exec(channel) {
        const LoggingChannel = this.client.channels.cache.get("831299355943632968");
        return LoggingChannel.send(new discord_js_1.MessageEmbed()
            .setTitle(`Channel Deleted`)
            .setDescription(`Channel Deleted: ${channel.name}`)
            .setColor('#0843cc'));
    }
}
exports.default = ChannelDeleteListener;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhbm5lbERlbGV0ZUxpc3RlbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpc3RlbmVycy9jbGllbnQvY2hhbm5lbERlbGV0ZUxpc3RlbmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbURBQTBDO0FBQzFDLDJDQUFnRTtBQUVoRSxNQUFxQixxQkFBc0IsU0FBUSx5QkFBUTtJQUV2RDtRQUNJLEtBQUssQ0FBQyxlQUFlLEVBQUU7WUFDbkIsS0FBSyxFQUFFLGVBQWU7WUFDdEIsT0FBTyxFQUFFLFFBQVE7WUFDakIsUUFBUSxFQUFFLFFBQVE7U0FDckIsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVNLElBQUksQ0FBQyxPQUFvQjtRQUU1QixNQUFNLGNBQWMsR0FBZ0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBZ0IsQ0FBQztRQUV4RyxPQUFPLGNBQWMsQ0FBQyxJQUFJLENBQ3RCLElBQUkseUJBQVksRUFBRTthQUNqQixRQUFRLENBQUMsaUJBQWlCLENBQUM7YUFDM0IsY0FBYyxDQUFDLG9CQUFvQixPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDbEQsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUN2QixDQUFBO0lBQ0wsQ0FBQztDQUNKO0FBckJELHdDQXFCQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IExpc3RlbmVyIH0gZnJvbSBcImRpc2NvcmQtYWthaXJvXCI7XG5pbXBvcnQgeyBNZXNzYWdlLCBNZXNzYWdlRW1iZWQsIFRleHRDaGFubmVsIH0gZnJvbSBcImRpc2NvcmQuanNcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2hhbm5lbERlbGV0ZUxpc3RlbmVyIGV4dGVuZHMgTGlzdGVuZXIge1xuXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcihcImNoYW5uZWxEZWxldGVcIiwge1xuICAgICAgICAgICAgZXZlbnQ6IFwiY2hhbm5lbERlbGV0ZVwiLFxuICAgICAgICAgICAgZW1pdHRlcjogXCJjbGllbnRcIixcbiAgICAgICAgICAgIGNhdGVnb3J5OiBcImNsaWVudFwiXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgcHVibGljIGV4ZWMoY2hhbm5lbDogVGV4dENoYW5uZWwpOiBQcm9taXNlPE1lc3NhZ2U+IHtcblxuICAgICAgICBjb25zdCBMb2dnaW5nQ2hhbm5lbDogVGV4dENoYW5uZWwgPSB0aGlzLmNsaWVudC5jaGFubmVscy5jYWNoZS5nZXQoXCI4MzEyOTkzNTU5NDM2MzI5NjhcIikgYXMgVGV4dENoYW5uZWw7XG5cbiAgICAgICAgcmV0dXJuIExvZ2dpbmdDaGFubmVsLnNlbmQoXG4gICAgICAgICAgICBuZXcgTWVzc2FnZUVtYmVkKClcbiAgICAgICAgICAgIC5zZXRUaXRsZShgQ2hhbm5lbCBEZWxldGVkYClcbiAgICAgICAgICAgIC5zZXREZXNjcmlwdGlvbihgQ2hhbm5lbCBEZWxldGVkOiAke2NoYW5uZWwubmFtZX1gKVxuICAgICAgICAgICAgLnNldENvbG9yKCcjMDg0M2NjJylcbiAgICAgICAgKVxuICAgIH1cbn0iXX0=