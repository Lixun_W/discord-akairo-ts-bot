"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
class ChannelCreateListener extends discord_akairo_1.Listener {
    constructor() {
        super("channelCreate", {
            event: "channelCreate",
            emitter: "client",
            category: "client"
        });
    }
    async exec(channel) {
        const LoggingChannel = this.client.channels.cache.get("831319698930532392");
        return LoggingChannel.send(new discord_js_1.MessageEmbed()
            .setTitle(`Channel Created`)
            .setDescription(`Channel Created: ${channel.name}`)
            .setColor('#0843cc'));
    }
}
exports.default = ChannelCreateListener;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhbm5lbENyZWF0ZUxpc3RlbmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpc3RlbmVycy9jbGllbnQvY2hhbm5lbENyZWF0ZUxpc3RlbmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbURBQTBDO0FBQzFDLDJDQUFnRTtBQUVoRSxNQUFxQixxQkFBc0IsU0FBUSx5QkFBUTtJQUV2RDtRQUNJLEtBQUssQ0FBQyxlQUFlLEVBQUU7WUFDbkIsS0FBSyxFQUFFLGVBQWU7WUFDdEIsT0FBTyxFQUFFLFFBQVE7WUFDakIsUUFBUSxFQUFFLFFBQVE7U0FDckIsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVNLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBb0I7UUFFbEMsTUFBTSxjQUFjLEdBQWdCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQWdCLENBQUM7UUFFeEcsT0FBTyxjQUFjLENBQUMsSUFBSSxDQUN0QixJQUFJLHlCQUFZLEVBQUU7YUFDakIsUUFBUSxDQUFDLGlCQUFpQixDQUFDO2FBQzNCLGNBQWMsQ0FBQyxvQkFBb0IsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ2xELFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FDdkIsQ0FBQTtJQUNMLENBQUM7Q0FDSjtBQXJCRCx3Q0FxQkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBMaXN0ZW5lciB9IGZyb20gXCJkaXNjb3JkLWFrYWlyb1wiO1xuaW1wb3J0IHsgTWVzc2FnZSwgTWVzc2FnZUVtYmVkLCBUZXh0Q2hhbm5lbCB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENoYW5uZWxDcmVhdGVMaXN0ZW5lciBleHRlbmRzIExpc3RlbmVyIHtcblxuICAgIHB1YmxpYyBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoXCJjaGFubmVsQ3JlYXRlXCIsIHtcbiAgICAgICAgICAgIGV2ZW50OiBcImNoYW5uZWxDcmVhdGVcIixcbiAgICAgICAgICAgIGVtaXR0ZXI6IFwiY2xpZW50XCIsXG4gICAgICAgICAgICBjYXRlZ29yeTogXCJjbGllbnRcIlxuICAgICAgICB9KVxuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyBleGVjKGNoYW5uZWw6IFRleHRDaGFubmVsKTogUHJvbWlzZTxNZXNzYWdlPiB7XG5cbiAgICAgICAgY29uc3QgTG9nZ2luZ0NoYW5uZWw6IFRleHRDaGFubmVsID0gdGhpcy5jbGllbnQuY2hhbm5lbHMuY2FjaGUuZ2V0KFwiODMxMzE5Njk4OTMwNTMyMzkyXCIpIGFzIFRleHRDaGFubmVsO1xuXG4gICAgICAgIHJldHVybiBMb2dnaW5nQ2hhbm5lbC5zZW5kKFxuICAgICAgICAgICAgbmV3IE1lc3NhZ2VFbWJlZCgpXG4gICAgICAgICAgICAuc2V0VGl0bGUoYENoYW5uZWwgQ3JlYXRlZGApXG4gICAgICAgICAgICAuc2V0RGVzY3JpcHRpb24oYENoYW5uZWwgQ3JlYXRlZDogJHtjaGFubmVsLm5hbWV9YClcbiAgICAgICAgICAgIC5zZXRDb2xvcignIzA4NDNjYycpXG4gICAgICAgIClcbiAgICB9XG59Il19