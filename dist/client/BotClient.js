"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const path_1 = require("path");
const config_1 = require("../config");
const Database_1 = __importDefault(require("../Structures/Database"));
class BotClient extends discord_akairo_1.AkairoClient {
    constructor(config) {
        super({
            ownerID: config.owners
        });
        this.listenerHandler = new discord_akairo_1.ListenerHandler(this, {
            directory: path_1.join(__dirname, "..", "listeners")
        });
        this.commandHandler = new discord_akairo_1.CommandHandler(this, {
            directory: path_1.join(__dirname, "..", "commands"),
            prefix: config_1.prefix,
            allowMention: true,
            handleEdits: true,
            commandUtil: true,
            commandUtilLifetime: 3e5,
            defaultCooldown: 1000,
            argumentDefaults: {
                prompt: {
                    modifyStart: (_, str) => `${str}\n\nType \`cancel\` to cancel the command...`,
                    modifyRetry: (_, str) => `${str}\n\nType \`cancel\` to cancel the command...`,
                    timeout: "You took too long, therefore the command has now been cancelled",
                    ended: "You exceeded the maximum amomunt of tries, this command has now been cancelled",
                    cancel: "Command cancelled",
                    retries: 3,
                    time: 3e4
                },
                otherwise: ""
            },
        });
        this.config = config;
    }
    async _init() {
        this.commandHandler.useListenerHandler(this.listenerHandler);
        this.listenerHandler.setEmitters({
            commandHandler: this.commandHandler,
            listenerHandler: this.listenerHandler,
            process
        });
        this.commandHandler.loadAll();
        this.listenerHandler.loadAll();
        this.db = Database_1.default.get(config_1.dbName);
        await this.db.connect();
        await this.db.synchronize();
    }
    async start() {
        await this._init();
        return this.login(this.config.token);
    }
}
exports.default = BotClient;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQm90Q2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NsaWVudC9Cb3RDbGllbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxtREFBK0U7QUFFL0UsK0JBQTRCO0FBQzVCLHNDQUFtRDtBQUVuRCxzRUFBOEM7QUFnQjlDLE1BQXFCLFNBQVUsU0FBUSw2QkFBWTtJQTRCL0MsWUFBbUIsTUFBa0I7UUFDakMsS0FBSyxDQUFDO1lBQ0YsT0FBTyxFQUFFLE1BQU0sQ0FBQyxNQUFNO1NBQ3pCLENBQUMsQ0FBQztRQTVCQSxvQkFBZSxHQUFvQixJQUFJLGdDQUFlLENBQUMsSUFBSSxFQUFFO1lBQ2hFLFNBQVMsRUFBRSxXQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxXQUFXLENBQUM7U0FDaEQsQ0FBQyxDQUFBO1FBQ0ssbUJBQWMsR0FBbUIsSUFBSSwrQkFBYyxDQUFDLElBQUksRUFBRTtZQUM3RCxTQUFTLEVBQUUsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDO1lBQzVDLE1BQU0sRUFBRSxlQUFNO1lBQ2QsWUFBWSxFQUFFLElBQUk7WUFDbEIsV0FBVyxFQUFFLElBQUk7WUFDakIsV0FBVyxFQUFFLElBQUk7WUFDakIsbUJBQW1CLEVBQUUsR0FBRztZQUN4QixlQUFlLEVBQUUsSUFBSTtZQUNyQixnQkFBZ0IsRUFBRTtnQkFDZCxNQUFNLEVBQUU7b0JBQ0osV0FBVyxFQUFFLENBQUMsQ0FBVSxFQUFFLEdBQVcsRUFBVSxFQUFFLENBQUMsR0FBRyxHQUFHLDhDQUE4QztvQkFDdEcsV0FBVyxFQUFFLENBQUMsQ0FBVSxFQUFFLEdBQVcsRUFBVSxFQUFFLENBQUMsR0FBRyxHQUFHLDhDQUE4QztvQkFDdEcsT0FBTyxFQUFFLGlFQUFpRTtvQkFDMUUsS0FBSyxFQUFFLGdGQUFnRjtvQkFDdkYsTUFBTSxFQUFFLG1CQUFtQjtvQkFDM0IsT0FBTyxFQUFFLENBQUM7b0JBQ1YsSUFBSSxFQUFFLEdBQUc7aUJBQ1o7Z0JBQ0QsU0FBUyxFQUFFLEVBQUU7YUFDaEI7U0FDSixDQUFDLENBQUM7UUFPQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN6QixDQUFDO0lBRU8sS0FBSyxDQUFDLEtBQUs7UUFDZixJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQztZQUM3QixjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWM7WUFDbkMsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlO1lBQ3JDLE9BQU87U0FDVixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFL0IsSUFBSSxDQUFDLEVBQUUsR0FBRyxrQkFBUSxDQUFDLEdBQUcsQ0FBQyxlQUFNLENBQUMsQ0FBQztRQUMvQixNQUFNLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDeEIsTUFBTSxJQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFTSxLQUFLLENBQUMsS0FBSztRQUNkLE1BQU0sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25CLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Q0FDSjtBQXhERCw0QkF3REMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBa2Fpcm9DbGllbnQsIENvbW1hbmRIYW5kbGVyLCBMaXN0ZW5lckhhbmRsZXIgfSBmcm9tIFwiZGlzY29yZC1ha2Fpcm9cIjtcbmltcG9ydCB7IE1lc3NhZ2UgfSBmcm9tIFwiZGlzY29yZC5qc1wiO1xuaW1wb3J0IHsgam9pbiB9IGZyb20gXCJwYXRoXCI7XG5pbXBvcnQgeyBwcmVmaXgsIG93bmVycywgZGJOYW1lIH0gZnJvbSBcIi4uL2NvbmZpZ1wiO1xuaW1wb3J0IHsgQ29ubmVjdGlvbiB9IGZyb20gXCJ0eXBlb3JtXCI7XG5pbXBvcnQgRGF0YWJhc2UgZnJvbSBcIi4uL1N0cnVjdHVyZXMvRGF0YWJhc2VcIjtcblxuXG5kZWNsYXJlIG1vZHVsZSBcImRpc2NvcmQtYWthaXJvXCIge1xuICAgIGludGVyZmFjZSBBa2Fpcm9DbGllbnQge1xuICAgICAgICBjb21tYW5kSGFuZGxlcjogQ29tbWFuZEhhbmRsZXI7XG4gICAgICAgIGxpc3RlbmVySGFuZGxlcjogTGlzdGVuZXJIYW5kbGVyO1xuICAgICAgICBkYjogQ29ubmVjdGlvbjtcbiAgICB9XG59XG5cbmludGVyZmFjZSBCb3RPcHRpb25zIHtcbiAgICB0b2tlbj86IHN0cmluZztcbiAgICBvd25lcnM/OiBzdHJpbmcgfCBzdHJpbmdbXVxufVxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCb3RDbGllbnQgZXh0ZW5kcyBBa2Fpcm9DbGllbnQge1xuICAgIHB1YmxpYyBjb25maWc6IEJvdE9wdGlvbnM7XG4gICAgcHVibGljIGRiITogQ29ubmVjdGlvbjtcbiAgICBwdWJsaWMgbGlzdGVuZXJIYW5kbGVyOiBMaXN0ZW5lckhhbmRsZXIgPSBuZXcgTGlzdGVuZXJIYW5kbGVyKHRoaXMsIHtcbiAgICAgICAgZGlyZWN0b3J5OiBqb2luKF9fZGlybmFtZSwgXCIuLlwiLCBcImxpc3RlbmVyc1wiKVxuICAgIH0pXG4gICAgcHVibGljIGNvbW1hbmRIYW5kbGVyOiBDb21tYW5kSGFuZGxlciA9IG5ldyBDb21tYW5kSGFuZGxlcih0aGlzLCB7XG4gICAgICAgIGRpcmVjdG9yeTogam9pbihfX2Rpcm5hbWUsIFwiLi5cIiwgXCJjb21tYW5kc1wiKSxcbiAgICAgICAgcHJlZml4OiBwcmVmaXgsXG4gICAgICAgIGFsbG93TWVudGlvbjogdHJ1ZSxcbiAgICAgICAgaGFuZGxlRWRpdHM6IHRydWUsXG4gICAgICAgIGNvbW1hbmRVdGlsOiB0cnVlLFxuICAgICAgICBjb21tYW5kVXRpbExpZmV0aW1lOiAzZTUsXG4gICAgICAgIGRlZmF1bHRDb29sZG93bjogMTAwMCxcbiAgICAgICAgYXJndW1lbnREZWZhdWx0czoge1xuICAgICAgICAgICAgcHJvbXB0OiB7XG4gICAgICAgICAgICAgICAgbW9kaWZ5U3RhcnQ6IChfOiBNZXNzYWdlLCBzdHI6IHN0cmluZyk6IHN0cmluZyA9PiBgJHtzdHJ9XFxuXFxuVHlwZSBcXGBjYW5jZWxcXGAgdG8gY2FuY2VsIHRoZSBjb21tYW5kLi4uYCxcbiAgICAgICAgICAgICAgICBtb2RpZnlSZXRyeTogKF86IE1lc3NhZ2UsIHN0cjogc3RyaW5nKTogc3RyaW5nID0+IGAke3N0cn1cXG5cXG5UeXBlIFxcYGNhbmNlbFxcYCB0byBjYW5jZWwgdGhlIGNvbW1hbmQuLi5gLFxuICAgICAgICAgICAgICAgIHRpbWVvdXQ6IFwiWW91IHRvb2sgdG9vIGxvbmcsIHRoZXJlZm9yZSB0aGUgY29tbWFuZCBoYXMgbm93IGJlZW4gY2FuY2VsbGVkXCIsXG4gICAgICAgICAgICAgICAgZW5kZWQ6IFwiWW91IGV4Y2VlZGVkIHRoZSBtYXhpbXVtIGFtb211bnQgb2YgdHJpZXMsIHRoaXMgY29tbWFuZCBoYXMgbm93IGJlZW4gY2FuY2VsbGVkXCIsXG4gICAgICAgICAgICAgICAgY2FuY2VsOiBcIkNvbW1hbmQgY2FuY2VsbGVkXCIsXG4gICAgICAgICAgICAgICAgcmV0cmllczogMyxcbiAgICAgICAgICAgICAgICB0aW1lOiAzZTRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBvdGhlcndpc2U6IFwiXCJcbiAgICAgICAgfSxcbiAgICB9KTtcblxuICAgIHB1YmxpYyBjb25zdHJ1Y3Rvcihjb25maWc6IEJvdE9wdGlvbnMpIHtcbiAgICAgICAgc3VwZXIoe1xuICAgICAgICAgICAgb3duZXJJRDogY29uZmlnLm93bmVyc1xuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmNvbmZpZyA9IGNvbmZpZztcbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIF9pbml0KCk6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICB0aGlzLmNvbW1hbmRIYW5kbGVyLnVzZUxpc3RlbmVySGFuZGxlcih0aGlzLmxpc3RlbmVySGFuZGxlcik7XG4gICAgICAgIHRoaXMubGlzdGVuZXJIYW5kbGVyLnNldEVtaXR0ZXJzKHtcbiAgICAgICAgICAgIGNvbW1hbmRIYW5kbGVyOiB0aGlzLmNvbW1hbmRIYW5kbGVyLFxuICAgICAgICAgICAgbGlzdGVuZXJIYW5kbGVyOiB0aGlzLmxpc3RlbmVySGFuZGxlcixcbiAgICAgICAgICAgIHByb2Nlc3NcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5jb21tYW5kSGFuZGxlci5sb2FkQWxsKCk7XG4gICAgICAgIHRoaXMubGlzdGVuZXJIYW5kbGVyLmxvYWRBbGwoKTtcblxuICAgICAgICB0aGlzLmRiID0gRGF0YWJhc2UuZ2V0KGRiTmFtZSk7XG4gICAgICAgIGF3YWl0IHRoaXMuZGIuY29ubmVjdCgpO1xuICAgICAgICBhd2FpdCB0aGlzLmRiLnN5bmNocm9uaXplKCk7XG4gICAgfVxuXG4gICAgcHVibGljIGFzeW5jIHN0YXJ0KCk6IFByb21pc2U8U3RyaW5nPiB7XG4gICAgICAgIGF3YWl0IHRoaXMuX2luaXQoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMubG9naW4odGhpcy5jb25maWcudG9rZW4pO1xuICAgIH1cbn0iXX0=