"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
class KickCommand extends discord_akairo_1.Command {
    constructor() {
        super("kick", {
            aliases: ['kick', 'k'],
            category: "Moderation Commands",
            description: {
                content: 'Kick someone from the server',
                usage: 'kick <Member> [Reason]',
                examples: [
                    'kick @Lukas',
                    'kick Lukas W'
                ]
            },
            userPermissions: ["KICK_MEMBERS"],
            args: [
                {
                    id: "member",
                    type: "member",
                    prompt: {
                        start: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to kick them!`,
                        retry: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to kick them!`
                    }
                },
                {
                    id: "reason",
                    type: "string",
                    match: "rest",
                    default: "No Reason Provided"
                }
            ]
        });
    }
    async exec(message, { member, reason }) {
        if (member.roles.highest.position >= message.member.roles.highest.position && message.author.id !== message.guild.ownerID)
            return message.util.send(new discord_js_1.MessageEmbed()
                .setTitle(`Error`)
                .setDescription(`This member is higher than you therefore a kick cannot be given`)
                .setTimestamp()
                .setColor('#0843cc'));
        member.kick();
        return message.util.send(new discord_js_1.MessageEmbed()
            .setTitle(`Kicked!`)
            .setDescription(`Kicked: ${member.user.tag}, Reason: ${reason}`)
            .setTimestamp()
            .setColor('#0843cc'));
    }
}
exports.default = KickCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiS2lja0NvbW1hbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29tbWFuZHMvTW9kZXJhdGlvbiBDb21tYW5kcy9LaWNrQ29tbWFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG1EQUF5QztBQUV6QywyQ0FBbUQ7QUFFbkQsTUFBcUIsV0FBWSxTQUFRLHdCQUFPO0lBRTVDO1FBQ0ksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNWLE9BQU8sRUFBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUM7WUFDdEIsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixXQUFXLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLDhCQUE4QjtnQkFDdkMsS0FBSyxFQUFFLHdCQUF3QjtnQkFDL0IsUUFBUSxFQUFFO29CQUNOLGFBQWE7b0JBQ2IsY0FBYztpQkFDakI7YUFDSjtZQUNELGVBQWUsRUFBRSxDQUFDLGNBQWMsQ0FBQztZQUNqQyxJQUFJLEVBQUU7Z0JBQ0Y7b0JBQ0ksRUFBRSxFQUFFLFFBQVE7b0JBQ1osSUFBSSxFQUFFLFFBQVE7b0JBQ2QsTUFBTSxFQUFFO3dCQUNKLEtBQUssRUFBRSxDQUFDLEdBQVksRUFBRSxFQUFFLENBQUMsMEZBQTBGO3dCQUNuSCxLQUFLLEVBQUUsQ0FBQyxHQUFZLEVBQUUsRUFBRSxDQUFDLDBGQUEwRjtxQkFDdEg7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksRUFBRSxFQUFFLFFBQVE7b0JBQ1osSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLE1BQU07b0JBQ2IsT0FBTyxFQUFFLG9CQUFvQjtpQkFDaEM7YUFDSjtTQUNKLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQWdCLEVBQUUsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUEyQztRQUUzRixJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxLQUFLLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTztZQUN6SCxPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNwQixJQUFJLHlCQUFZLEVBQUU7aUJBQ2pCLFFBQVEsQ0FBQyxPQUFPLENBQUM7aUJBQ2pCLGNBQWMsQ0FBQyxpRUFBaUUsQ0FBQztpQkFDakYsWUFBWSxFQUFFO2lCQUNkLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FDdkIsQ0FBQTtRQUVELE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtRQUViLE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ3BCLElBQUkseUJBQVksRUFBRTthQUNqQixRQUFRLENBQUMsU0FBUyxDQUFDO2FBQ25CLGNBQWMsQ0FBQyxXQUFXLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxhQUFhLE1BQU0sRUFBRSxDQUFDO2FBQy9ELFlBQVksRUFBRTthQUNkLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FDdkIsQ0FBQTtJQUNMLENBQUM7Q0FDSjtBQXZERCw4QkF1REMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tYW5kIH0gZnJvbSBcImRpc2NvcmQtYWthaXJvXCI7XG5pbXBvcnQgeyBHdWlsZE1lbWJlciB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XG5pbXBvcnQgeyBNZXNzYWdlLCBNZXNzYWdlRW1iZWQgfSBmcm9tIFwiZGlzY29yZC5qc1wiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBLaWNrQ29tbWFuZCBleHRlbmRzIENvbW1hbmQge1xuXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcihcImtpY2tcIiwge1xuICAgICAgICAgICAgYWxpYXNlczogWydraWNrJywgJ2snXSxcbiAgICAgICAgICAgIGNhdGVnb3J5OiBcIk1vZGVyYXRpb24gQ29tbWFuZHNcIixcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiB7XG4gICAgICAgICAgICAgICAgY29udGVudDogJ0tpY2sgc29tZW9uZSBmcm9tIHRoZSBzZXJ2ZXInLFxuICAgICAgICAgICAgICAgIHVzYWdlOiAna2ljayA8TWVtYmVyPiBbUmVhc29uXScsXG4gICAgICAgICAgICAgICAgZXhhbXBsZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgJ2tpY2sgQEx1a2FzJyxcbiAgICAgICAgICAgICAgICAgICAgJ2tpY2sgTHVrYXMgVydcbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdXNlclBlcm1pc3Npb25zOiBbXCJLSUNLX01FTUJFUlNcIl0sXG4gICAgICAgICAgICBhcmdzOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDogXCJtZW1iZXJcIixcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJtZW1iZXJcIixcbiAgICAgICAgICAgICAgICAgICAgcHJvbXB0OiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGFydDogKG1zZzogTWVzc2FnZSkgPT4gYDw6Y3Jvc3N0aW5neTo3OTgzMDkyNDc3NTc3MTM0MDg+IEluY29ycmVjdCB1c2FnZSFcXG4gUGxlYXNlIG1lbnRpb24gc29tZW9uZSB0byBraWNrIHRoZW0hYCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHJ5OiAobXNnOiBNZXNzYWdlKSA9PiBgPDpjcm9zc3Rpbmd5Ojc5ODMwOTI0Nzc1NzcxMzQwOD4gSW5jb3JyZWN0IHVzYWdlIVxcbiBQbGVhc2UgbWVudGlvbiBzb21lb25lIHRvIGtpY2sgdGhlbSFgXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6IFwicmVhc29uXCIsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICAgICAgICAgIG1hdGNoOiBcInJlc3RcIixcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDogXCJObyBSZWFzb24gUHJvdmlkZWRcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgZXhlYyhtZXNzYWdlOiBNZXNzYWdlLCB7IG1lbWJlciwgcmVhc29uIH06IHsgbWVtYmVyOiBHdWlsZE1lbWJlciwgcmVhc29uOiBzdHJpbmcgfSk6IFByb21pc2U8TWVzc2FnZT4ge1xuXG4gICAgICAgIGlmIChtZW1iZXIucm9sZXMuaGlnaGVzdC5wb3NpdGlvbiA+PSBtZXNzYWdlLm1lbWJlci5yb2xlcy5oaWdoZXN0LnBvc2l0aW9uICYmIG1lc3NhZ2UuYXV0aG9yLmlkICE9PSBtZXNzYWdlLmd1aWxkLm93bmVySUQpXG4gICAgICAgIHJldHVybiBtZXNzYWdlLnV0aWwuc2VuZChcbiAgICAgICAgICAgIG5ldyBNZXNzYWdlRW1iZWQoKVxuICAgICAgICAgICAgLnNldFRpdGxlKGBFcnJvcmApXG4gICAgICAgICAgICAuc2V0RGVzY3JpcHRpb24oYFRoaXMgbWVtYmVyIGlzIGhpZ2hlciB0aGFuIHlvdSB0aGVyZWZvcmUgYSBraWNrIGNhbm5vdCBiZSBnaXZlbmApXG4gICAgICAgICAgICAuc2V0VGltZXN0YW1wKClcbiAgICAgICAgICAgIC5zZXRDb2xvcignIzA4NDNjYycpXG4gICAgICAgIClcblxuICAgICAgICBtZW1iZXIua2ljaygpXG5cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2UudXRpbC5zZW5kKFxuICAgICAgICAgICAgbmV3IE1lc3NhZ2VFbWJlZCgpXG4gICAgICAgICAgICAuc2V0VGl0bGUoYEtpY2tlZCFgKVxuICAgICAgICAgICAgLnNldERlc2NyaXB0aW9uKGBLaWNrZWQ6ICR7bWVtYmVyLnVzZXIudGFnfSwgUmVhc29uOiAke3JlYXNvbn1gKVxuICAgICAgICAgICAgLnNldFRpbWVzdGFtcCgpXG4gICAgICAgICAgICAuc2V0Q29sb3IoJyMwODQzY2MnKVxuICAgICAgICApXG4gICAgfVxufSJdfQ==