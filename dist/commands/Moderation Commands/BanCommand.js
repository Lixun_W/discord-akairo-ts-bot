"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
class BanCommand extends discord_akairo_1.Command {
    constructor() {
        super("ban", {
            aliases: ['ban', 'b'],
            category: "Moderation Commands",
            description: {
                content: 'Ban someone from the server',
                usage: 'ban <Member> [Reason]',
                examples: [
                    'ban @Lukas',
                    'ban Lukas W'
                ]
            },
            userPermissions: ["BAN_MEMBERS"],
            args: [
                {
                    id: "member",
                    type: "member",
                    prompt: {
                        start: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to ban them!`,
                        retry: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to ban them!`
                    }
                },
                {
                    id: "reason",
                    type: "string",
                    match: "rest",
                    default: "No Reason Provided"
                }
            ]
        });
    }
    async exec(message, { member, reason }) {
        if (member.roles.highest.position >= message.member.roles.highest.position && message.author.id !== message.guild.ownerID)
            return message.util.send(new discord_js_1.MessageEmbed()
                .setTitle(`Error`)
                .setDescription(`This member is higher than you therefore a ban cannot be given`)
                .setTimestamp()
                .setColor('#0843cc'));
        member.ban();
        return message.util.send(new discord_js_1.MessageEmbed()
            .setTitle(`Banned!`)
            .setDescription(`Banned: ${member.user.tag}, Reason: ${reason}`)
            .setTimestamp()
            .setColor('#0843cc'));
    }
}
exports.default = BanCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQmFuQ29tbWFuZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21tYW5kcy9Nb2RlcmF0aW9uIENvbW1hbmRzL0JhbkNvbW1hbmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxtREFBeUM7QUFFekMsMkNBQW1EO0FBRW5ELE1BQXFCLFVBQVcsU0FBUSx3QkFBTztJQUUzQztRQUNJLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDVCxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDO1lBQ3JCLFFBQVEsRUFBRSxxQkFBcUI7WUFDL0IsV0FBVyxFQUFFO2dCQUNULE9BQU8sRUFBRSw2QkFBNkI7Z0JBQ3RDLEtBQUssRUFBRSx1QkFBdUI7Z0JBQzlCLFFBQVEsRUFBRTtvQkFDTixZQUFZO29CQUNaLGFBQWE7aUJBQ2hCO2FBQ0o7WUFDRCxlQUFlLEVBQUUsQ0FBQyxhQUFhLENBQUM7WUFDaEMsSUFBSSxFQUFFO2dCQUNGO29CQUNJLEVBQUUsRUFBRSxRQUFRO29CQUNaLElBQUksRUFBRSxRQUFRO29CQUNkLE1BQU0sRUFBRTt3QkFDSixLQUFLLEVBQUUsQ0FBQyxHQUFZLEVBQUUsRUFBRSxDQUFDLHlGQUF5Rjt3QkFDbEgsS0FBSyxFQUFFLENBQUMsR0FBWSxFQUFFLEVBQUUsQ0FBQyx5RkFBeUY7cUJBQ3JIO2lCQUNKO2dCQUNEO29CQUNJLEVBQUUsRUFBRSxRQUFRO29CQUNaLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxvQkFBb0I7aUJBQ2hDO2FBQ0o7U0FDSixDQUFDLENBQUE7SUFDTixDQUFDO0lBRU0sS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFnQixFQUFFLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBMkM7UUFFM0YsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU87WUFDekgsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDcEIsSUFBSSx5QkFBWSxFQUFFO2lCQUNqQixRQUFRLENBQUMsT0FBTyxDQUFDO2lCQUNqQixjQUFjLENBQUMsZ0VBQWdFLENBQUM7aUJBQ2hGLFlBQVksRUFBRTtpQkFDZCxRQUFRLENBQUMsU0FBUyxDQUFDLENBQ3ZCLENBQUE7UUFFRCxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUE7UUFFWixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNwQixJQUFJLHlCQUFZLEVBQUU7YUFDakIsUUFBUSxDQUFDLFNBQVMsQ0FBQzthQUNuQixjQUFjLENBQUMsV0FBVyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsYUFBYSxNQUFNLEVBQUUsQ0FBQzthQUMvRCxZQUFZLEVBQUU7YUFDZCxRQUFRLENBQUMsU0FBUyxDQUFDLENBQ3ZCLENBQUE7SUFDTCxDQUFDO0NBQ0o7QUF2REQsNkJBdURDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbWFuZCB9IGZyb20gXCJkaXNjb3JkLWFrYWlyb1wiO1xuaW1wb3J0IHsgR3VpbGRNZW1iZXIgfSBmcm9tIFwiZGlzY29yZC5qc1wiO1xuaW1wb3J0IHsgTWVzc2FnZSwgTWVzc2FnZUVtYmVkIH0gZnJvbSBcImRpc2NvcmQuanNcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFuQ29tbWFuZCBleHRlbmRzIENvbW1hbmQge1xuXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcihcImJhblwiLCB7XG4gICAgICAgICAgICBhbGlhc2VzOiBbJ2JhbicsICdiJ10sXG4gICAgICAgICAgICBjYXRlZ29yeTogXCJNb2RlcmF0aW9uIENvbW1hbmRzXCIsXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjoge1xuICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICdCYW4gc29tZW9uZSBmcm9tIHRoZSBzZXJ2ZXInLFxuICAgICAgICAgICAgICAgIHVzYWdlOiAnYmFuIDxNZW1iZXI+IFtSZWFzb25dJyxcbiAgICAgICAgICAgICAgICBleGFtcGxlczogW1xuICAgICAgICAgICAgICAgICAgICAnYmFuIEBMdWthcycsXG4gICAgICAgICAgICAgICAgICAgICdiYW4gTHVrYXMgVydcbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdXNlclBlcm1pc3Npb25zOiBbXCJCQU5fTUVNQkVSU1wiXSxcbiAgICAgICAgICAgIGFyZ3M6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOiBcIm1lbWJlclwiLFxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcIm1lbWJlclwiLFxuICAgICAgICAgICAgICAgICAgICBwcm9tcHQ6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0OiAobXNnOiBNZXNzYWdlKSA9PiBgPDpjcm9zc3Rpbmd5Ojc5ODMwOTI0Nzc1NzcxMzQwOD4gSW5jb3JyZWN0IHVzYWdlIVxcbiBQbGVhc2UgbWVudGlvbiBzb21lb25lIHRvIGJhbiB0aGVtIWAsXG4gICAgICAgICAgICAgICAgICAgICAgICByZXRyeTogKG1zZzogTWVzc2FnZSkgPT4gYDw6Y3Jvc3N0aW5neTo3OTgzMDkyNDc3NTc3MTM0MDg+IEluY29ycmVjdCB1c2FnZSFcXG4gUGxlYXNlIG1lbnRpb24gc29tZW9uZSB0byBiYW4gdGhlbSFgXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6IFwicmVhc29uXCIsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICAgICAgICAgIG1hdGNoOiBcInJlc3RcIixcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDogXCJObyBSZWFzb24gUHJvdmlkZWRcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgZXhlYyhtZXNzYWdlOiBNZXNzYWdlLCB7IG1lbWJlciwgcmVhc29uIH06IHsgbWVtYmVyOiBHdWlsZE1lbWJlciwgcmVhc29uOiBzdHJpbmcgfSk6IFByb21pc2U8TWVzc2FnZT4ge1xuXG4gICAgICAgIGlmIChtZW1iZXIucm9sZXMuaGlnaGVzdC5wb3NpdGlvbiA+PSBtZXNzYWdlLm1lbWJlci5yb2xlcy5oaWdoZXN0LnBvc2l0aW9uICYmIG1lc3NhZ2UuYXV0aG9yLmlkICE9PSBtZXNzYWdlLmd1aWxkLm93bmVySUQpXG4gICAgICAgIHJldHVybiBtZXNzYWdlLnV0aWwuc2VuZChcbiAgICAgICAgICAgIG5ldyBNZXNzYWdlRW1iZWQoKVxuICAgICAgICAgICAgLnNldFRpdGxlKGBFcnJvcmApXG4gICAgICAgICAgICAuc2V0RGVzY3JpcHRpb24oYFRoaXMgbWVtYmVyIGlzIGhpZ2hlciB0aGFuIHlvdSB0aGVyZWZvcmUgYSBiYW4gY2Fubm90IGJlIGdpdmVuYClcbiAgICAgICAgICAgIC5zZXRUaW1lc3RhbXAoKVxuICAgICAgICAgICAgLnNldENvbG9yKCcjMDg0M2NjJylcbiAgICAgICAgKVxuXG4gICAgICAgIG1lbWJlci5iYW4oKVxuXG4gICAgICAgIHJldHVybiBtZXNzYWdlLnV0aWwuc2VuZChcbiAgICAgICAgICAgIG5ldyBNZXNzYWdlRW1iZWQoKVxuICAgICAgICAgICAgLnNldFRpdGxlKGBCYW5uZWQhYClcbiAgICAgICAgICAgIC5zZXREZXNjcmlwdGlvbihgQmFubmVkOiAke21lbWJlci51c2VyLnRhZ30sIFJlYXNvbjogJHtyZWFzb259YClcbiAgICAgICAgICAgIC5zZXRUaW1lc3RhbXAoKVxuICAgICAgICAgICAgLnNldENvbG9yKCcjMDg0M2NjJylcbiAgICAgICAgKVxuICAgIH1cbn0iXX0=