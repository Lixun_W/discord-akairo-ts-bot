"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
const Warns_1 = require("../../Models/Warns");
class WarnCommand extends discord_akairo_1.Command {
    constructor() {
        super("warn", {
            aliases: ["w"],
            category: "Moderation Commands",
            description: {
                content: "Warn a member for something they have done",
                usage: "warn <Member> <Reason>",
                examples: [
                    "warn @Lukas Being bad", ,
                    "warn lukas being bad"
                ]
            },
            userPermissions: ["MANAGE_MESSAGES"],
            args: [
                {
                    id: "member",
                    type: "member",
                    prompt: {
                        start: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to warn them!`,
                        retry: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to warn them!`
                    }
                },
                {
                    id: "reason",
                    type: "string",
                    match: "rest",
                    default: "No Reason Provided"
                }
            ]
        });
    }
    async exec(message, { member, reason }) {
        const warnRepo = this.client.db.getRepository(Warns_1.Warns);
        if (member.roles.highest.position >= message.member.roles.highest.position && message.author.id !== message.guild.ownerID)
            return message.util.send(new discord_js_1.MessageEmbed()
                .setTitle(`Error`)
                .setDescription(`This member is higher than you therefore a warn cannot be given`)
                .setTimestamp()
                .setColor('#0843cc'));
        await warnRepo.insert({
            guild: message.guild.id,
            user: member.id,
            moderator: message.author.id,
            reason: reason
        });
        return message.util.send(new discord_js_1.MessageEmbed()
            .setTitle(`Warned`)
            .setDescription(`${member.user.tag} Has Been Warned By ${message.author.tag} For ${reason}`)
            .setColor('#0843cc'));
    }
}
exports.default = WarnCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiV2FybkNvbW1hbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29tbWFuZHMvTW9kZXJhdGlvbiBDb21tYW5kcy9XYXJuQ29tbWFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG1EQUF5QztBQUV6QywyQ0FBZ0U7QUFFaEUsOENBQTJDO0FBRTNDLE1BQXFCLFdBQVksU0FBUSx3QkFBTztJQUM1QztRQUNJLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDVixPQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDZCxRQUFRLEVBQUUscUJBQXFCO1lBQy9CLFdBQVcsRUFBRTtnQkFDVCxPQUFPLEVBQUUsNENBQTRDO2dCQUNyRCxLQUFLLEVBQUUsd0JBQXdCO2dCQUMvQixRQUFRLEVBQUU7b0JBQ04sdUJBQXVCLEVBQUM7b0JBQ3hCLHNCQUFzQjtpQkFDekI7YUFDSjtZQUNELGVBQWUsRUFBRSxDQUFDLGlCQUFpQixDQUFDO1lBQ3BDLElBQUksRUFBRTtnQkFDRjtvQkFDSSxFQUFFLEVBQUUsUUFBUTtvQkFDWixJQUFJLEVBQUUsUUFBUTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osS0FBSyxFQUFFLENBQUMsR0FBWSxFQUFFLEVBQUUsQ0FBQywwRkFBMEY7d0JBQ25ILEtBQUssRUFBRSxDQUFDLEdBQVksRUFBRSxFQUFFLENBQUMsMEZBQTBGO3FCQUN0SDtpQkFDSjtnQkFDRDtvQkFDSSxFQUFFLEVBQUUsUUFBUTtvQkFDWixJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsTUFBTTtvQkFDYixPQUFPLEVBQUUsb0JBQW9CO2lCQUNoQzthQUNKO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBZ0IsRUFBRSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQTJDO1FBRTNGLE1BQU0sUUFBUSxHQUFzQixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsYUFBSyxDQUFDLENBQUM7UUFFeEUsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU87WUFDekgsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDcEIsSUFBSSx5QkFBWSxFQUFFO2lCQUNqQixRQUFRLENBQUMsT0FBTyxDQUFDO2lCQUNqQixjQUFjLENBQUMsaUVBQWlFLENBQUM7aUJBQ2pGLFlBQVksRUFBRTtpQkFDZCxRQUFRLENBQUMsU0FBUyxDQUFDLENBQ3ZCLENBQUE7UUFFRCxNQUFNLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDbEIsS0FBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN2QixJQUFJLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDZixTQUFTLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQzVCLE1BQU0sRUFBRSxNQUFNO1NBQ2pCLENBQUMsQ0FBQztRQUVILE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ3BCLElBQUkseUJBQVksRUFBRTthQUNqQixRQUFRLENBQUMsUUFBUSxDQUFDO2FBQ2xCLGNBQWMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyx1QkFBdUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLFFBQVEsTUFBTSxFQUFFLENBQUM7YUFDM0YsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUN2QixDQUFBO0lBQ0wsQ0FBQztDQUNKO0FBNURELDhCQTREQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1hbmQgfSBmcm9tIFwiZGlzY29yZC1ha2Fpcm9cIjtcbmltcG9ydCB7IEd1aWxkIH0gZnJvbSBcImRpc2NvcmQuanNcIjtcbmltcG9ydCB7IE1lc3NhZ2UsIEd1aWxkTWVtYmVyLCBNZXNzYWdlRW1iZWQgfSBmcm9tIFwiZGlzY29yZC5qc1wiO1xuaW1wb3J0IHsgUmVwb3NpdG9yeSB9IGZyb20gXCJ0eXBlb3JtXCI7XG5pbXBvcnQgeyBXYXJucyB9IGZyb20gXCIuLi8uLi9Nb2RlbHMvV2FybnNcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgV2FybkNvbW1hbmQgZXh0ZW5kcyBDb21tYW5kIHtcbiAgICBwdWJsaWMgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKFwid2FyblwiLCB7XG4gICAgICAgICAgICBhbGlhc2VzOiBbXCJ3XCJdLFxuICAgICAgICAgICAgY2F0ZWdvcnk6IFwiTW9kZXJhdGlvbiBDb21tYW5kc1wiLFxuICAgICAgICAgICAgZGVzY3JpcHRpb246IHtcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIldhcm4gYSBtZW1iZXIgZm9yIHNvbWV0aGluZyB0aGV5IGhhdmUgZG9uZVwiLFxuICAgICAgICAgICAgICAgIHVzYWdlOiBcIndhcm4gPE1lbWJlcj4gPFJlYXNvbj5cIixcbiAgICAgICAgICAgICAgICBleGFtcGxlczogW1xuICAgICAgICAgICAgICAgICAgICBcIndhcm4gQEx1a2FzIEJlaW5nIGJhZFwiLCxcbiAgICAgICAgICAgICAgICAgICAgXCJ3YXJuIGx1a2FzIGJlaW5nIGJhZFwiXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHVzZXJQZXJtaXNzaW9uczogW1wiTUFOQUdFX01FU1NBR0VTXCJdLFxuICAgICAgICAgICAgYXJnczogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6IFwibWVtYmVyXCIsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IFwibWVtYmVyXCIsXG4gICAgICAgICAgICAgICAgICAgIHByb21wdDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnQ6IChtc2c6IE1lc3NhZ2UpID0+IGA8OmNyb3NzdGluZ3k6Nzk4MzA5MjQ3NzU3NzEzNDA4PiBJbmNvcnJlY3QgdXNhZ2UhXFxuIFBsZWFzZSBtZW50aW9uIHNvbWVvbmUgdG8gd2FybiB0aGVtIWAsXG4gICAgICAgICAgICAgICAgICAgICAgICByZXRyeTogKG1zZzogTWVzc2FnZSkgPT4gYDw6Y3Jvc3N0aW5neTo3OTgzMDkyNDc3NTc3MTM0MDg+IEluY29ycmVjdCB1c2FnZSFcXG4gUGxlYXNlIG1lbnRpb24gc29tZW9uZSB0byB3YXJuIHRoZW0hYFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOiBcInJlYXNvblwiLFxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInN0cmluZ1wiLFxuICAgICAgICAgICAgICAgICAgICBtYXRjaDogXCJyZXN0XCIsXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6IFwiTm8gUmVhc29uIFByb3ZpZGVkXCJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyBleGVjKG1lc3NhZ2U6IE1lc3NhZ2UsIHsgbWVtYmVyLCByZWFzb24gfTogeyBtZW1iZXI6IEd1aWxkTWVtYmVyLCByZWFzb246IHN0cmluZyB9KTogUHJvbWlzZTxNZXNzYWdlPiB7XG5cbiAgICAgICAgY29uc3Qgd2FyblJlcG86IFJlcG9zaXRvcnk8V2FybnM+ID0gdGhpcy5jbGllbnQuZGIuZ2V0UmVwb3NpdG9yeShXYXJucyk7XG5cbiAgICAgICAgaWYgKG1lbWJlci5yb2xlcy5oaWdoZXN0LnBvc2l0aW9uID49IG1lc3NhZ2UubWVtYmVyLnJvbGVzLmhpZ2hlc3QucG9zaXRpb24gJiYgbWVzc2FnZS5hdXRob3IuaWQgIT09IG1lc3NhZ2UuZ3VpbGQub3duZXJJRClcbiAgICAgICAgcmV0dXJuIG1lc3NhZ2UudXRpbC5zZW5kKFxuICAgICAgICAgICAgbmV3IE1lc3NhZ2VFbWJlZCgpXG4gICAgICAgICAgICAuc2V0VGl0bGUoYEVycm9yYClcbiAgICAgICAgICAgIC5zZXREZXNjcmlwdGlvbihgVGhpcyBtZW1iZXIgaXMgaGlnaGVyIHRoYW4geW91IHRoZXJlZm9yZSBhIHdhcm4gY2Fubm90IGJlIGdpdmVuYClcbiAgICAgICAgICAgIC5zZXRUaW1lc3RhbXAoKVxuICAgICAgICAgICAgLnNldENvbG9yKCcjMDg0M2NjJylcbiAgICAgICAgKVxuICAgICAgICBcbiAgICAgICAgYXdhaXQgd2FyblJlcG8uaW5zZXJ0KHtcbiAgICAgICAgICAgIGd1aWxkOiBtZXNzYWdlLmd1aWxkLmlkLFxuICAgICAgICAgICAgdXNlcjogbWVtYmVyLmlkLFxuICAgICAgICAgICAgbW9kZXJhdG9yOiBtZXNzYWdlLmF1dGhvci5pZCxcbiAgICAgICAgICAgIHJlYXNvbjogcmVhc29uXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBtZXNzYWdlLnV0aWwuc2VuZChcbiAgICAgICAgICAgIG5ldyBNZXNzYWdlRW1iZWQoKVxuICAgICAgICAgICAgLnNldFRpdGxlKGBXYXJuZWRgKVxuICAgICAgICAgICAgLnNldERlc2NyaXB0aW9uKGAke21lbWJlci51c2VyLnRhZ30gSGFzIEJlZW4gV2FybmVkIEJ5ICR7bWVzc2FnZS5hdXRob3IudGFnfSBGb3IgJHtyZWFzb259YClcbiAgICAgICAgICAgIC5zZXRDb2xvcignIzA4NDNjYycpXG4gICAgICAgIClcbiAgICB9XG59Il19