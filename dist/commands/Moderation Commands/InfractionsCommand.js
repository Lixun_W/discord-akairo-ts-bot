"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
const Warns_1 = require("../../Models/Warns");
class InfractionsCommand extends discord_akairo_1.Command {
    constructor() {
        super("infractions", {
            aliases: ['punishments', 'warns'],
            category: "Moderation Commands",
            description: {
                content: "Check infractions of a member",
                usage: "infractions <Member>",
                examples: [
                    "infractions @Lukas",
                    "infractions lukas"
                ]
            },
            args: [
                {
                    id: "member",
                    type: "member",
                    default: (msg) => msg.member
                }
            ]
        });
    }
    async exec(message, { member }) {
        const warnRepo = this.client.db.getRepository(Warns_1.Warns);
        const warns = await warnRepo.find({ user: member.id, guild: message.guild.id });
        if (!warns.length) {
            return message.util.send(new discord_js_1.MessageEmbed()
                .setTitle(`Congrats!`)
                .setDescription(`${member.user.tag} does not have any warns inside my database`)
                .setTimestamp()
                .setColor('#0843cc'));
        }
        const infractions = await Promise.all(warns.map(async (v, i) => {
            const mod = await this.client.users.fetch(v.moderator).catch(() => null);
            if (mod)
                return {
                    index: i + 1,
                    moderator: mod.tag,
                    reason: v.reason,
                };
        }));
        return message.util.send(new discord_js_1.MessageEmbed()
            .setAuthor(`Infractions | ${member.user.username}`, member.user.displayAvatarURL({ dynamic: true }))
            .setColor('#0843cc')
            .setDescription(infractions.map(v => `\`#${v.index}\` | Moderator: __${v.moderator}__\nReason: *${v.reason}*\n`)));
    }
}
exports.default = InfractionsCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5mcmFjdGlvbnNDb21tYW5kLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL01vZGVyYXRpb24gQ29tbWFuZHMvSW5mcmFjdGlvbnNDb21tYW5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbURBQXlDO0FBQ3pDLDJDQUFzRTtBQUV0RSw4Q0FBMkM7QUFFM0MsTUFBcUIsa0JBQW1CLFNBQVEsd0JBQU87SUFDbkQ7UUFDSSxLQUFLLENBQUMsYUFBYSxFQUFFO1lBQ2pCLE9BQU8sRUFBRSxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUM7WUFDakMsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixXQUFXLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLCtCQUErQjtnQkFDeEMsS0FBSyxFQUFFLHNCQUFzQjtnQkFDN0IsUUFBUSxFQUFFO29CQUNOLG9CQUFvQjtvQkFDcEIsbUJBQW1CO2lCQUN0QjthQUNKO1lBQ0QsSUFBSSxFQUFFO2dCQUNGO29CQUNJLEVBQUUsRUFBRSxRQUFRO29CQUNaLElBQUksRUFBRSxRQUFRO29CQUNkLE9BQU8sRUFBRSxDQUFDLEdBQVksRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU07aUJBQ3hDO2FBQ0o7U0FDSixDQUFDLENBQUE7SUFDTixDQUFDO0lBRU0sS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFnQixFQUFFLEVBQUUsTUFBTSxFQUEyQjtRQUNuRSxNQUFNLFFBQVEsR0FBc0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQUssQ0FBQyxDQUFDO1FBRXhFLE1BQU0sS0FBSyxHQUFZLE1BQU0sUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFFekYsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNwQixJQUFJLHlCQUFZLEVBQUU7aUJBQ2pCLFFBQVEsQ0FBQyxXQUFXLENBQUM7aUJBQ3JCLGNBQWMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyw2Q0FBNkMsQ0FBQztpQkFDL0UsWUFBWSxFQUFFO2lCQUNkLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FDdkIsQ0FBQTtTQUNKO1FBRUQsTUFBTSxXQUFXLEdBQUcsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQVEsRUFBRSxDQUFTLEVBQUUsRUFBRTtZQUMxRSxNQUFNLEdBQUcsR0FBUyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9FLElBQUksR0FBRztnQkFBRSxPQUFPO29CQUNaLEtBQUssRUFBRSxDQUFDLEdBQUcsQ0FBQztvQkFDWixTQUFTLEVBQUUsR0FBRyxDQUFDLEdBQUc7b0JBQ2xCLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTTtpQkFDbkIsQ0FBQTtRQUNMLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFSixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNwQixJQUFJLHlCQUFZLEVBQUU7YUFDakIsU0FBUyxDQUFDLGlCQUFpQixNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzthQUNuRyxRQUFRLENBQUMsU0FBUyxDQUFDO2FBQ25CLGNBQWMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxxQkFBcUIsQ0FBQyxDQUFDLFNBQVMsZ0JBQWdCLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQ3BILENBQUE7SUFDTCxDQUFDO0NBQ0o7QUF0REQscUNBc0RDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbWFuZCB9IGZyb20gXCJkaXNjb3JkLWFrYWlyb1wiO1xuaW1wb3J0IHsgTWVzc2FnZSwgR3VpbGRNZW1iZXIsIFVzZXIsIE1lc3NhZ2VFbWJlZCB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XG5pbXBvcnQgeyBSZXBvc2l0b3J5IH0gZnJvbSBcInR5cGVvcm1cIjtcbmltcG9ydCB7IFdhcm5zIH0gZnJvbSBcIi4uLy4uL01vZGVscy9XYXJuc1wiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJbmZyYWN0aW9uc0NvbW1hbmQgZXh0ZW5kcyBDb21tYW5kIHtcbiAgICBwdWJsaWMgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKFwiaW5mcmFjdGlvbnNcIiwge1xuICAgICAgICAgICAgYWxpYXNlczogWydwdW5pc2htZW50cycsICd3YXJucyddLFxuICAgICAgICAgICAgY2F0ZWdvcnk6IFwiTW9kZXJhdGlvbiBDb21tYW5kc1wiLFxuICAgICAgICAgICAgZGVzY3JpcHRpb246IHtcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIkNoZWNrIGluZnJhY3Rpb25zIG9mIGEgbWVtYmVyXCIsXG4gICAgICAgICAgICAgICAgdXNhZ2U6IFwiaW5mcmFjdGlvbnMgPE1lbWJlcj5cIixcbiAgICAgICAgICAgICAgICBleGFtcGxlczogW1xuICAgICAgICAgICAgICAgICAgICBcImluZnJhY3Rpb25zIEBMdWthc1wiLFxuICAgICAgICAgICAgICAgICAgICBcImluZnJhY3Rpb25zIGx1a2FzXCJcbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYXJnczogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6IFwibWVtYmVyXCIsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IFwibWVtYmVyXCIsXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6IChtc2c6IE1lc3NhZ2UpID0+IG1zZy5tZW1iZXJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgcHVibGljIGFzeW5jIGV4ZWMobWVzc2FnZTogTWVzc2FnZSwgeyBtZW1iZXIgfTogeyBtZW1iZXI6IEd1aWxkTWVtYmVyIH0pOiBQcm9taXNlPE1lc3NhZ2U+IHtcbiAgICAgICAgY29uc3Qgd2FyblJlcG86IFJlcG9zaXRvcnk8V2FybnM+ID0gdGhpcy5jbGllbnQuZGIuZ2V0UmVwb3NpdG9yeShXYXJucyk7XG5cbiAgICAgICAgY29uc3Qgd2FybnM6IFdhcm5zW10gPSBhd2FpdCB3YXJuUmVwby5maW5kKHsgdXNlcjogbWVtYmVyLmlkLCBndWlsZDogbWVzc2FnZS5ndWlsZC5pZCB9KTtcblxuICAgICAgICBpZiAoIXdhcm5zLmxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuIG1lc3NhZ2UudXRpbC5zZW5kKFxuICAgICAgICAgICAgICAgIG5ldyBNZXNzYWdlRW1iZWQoKVxuICAgICAgICAgICAgICAgIC5zZXRUaXRsZShgQ29uZ3JhdHMhYClcbiAgICAgICAgICAgICAgICAuc2V0RGVzY3JpcHRpb24oYCR7bWVtYmVyLnVzZXIudGFnfSBkb2VzIG5vdCBoYXZlIGFueSB3YXJucyBpbnNpZGUgbXkgZGF0YWJhc2VgKVxuICAgICAgICAgICAgICAgIC5zZXRUaW1lc3RhbXAoKVxuICAgICAgICAgICAgICAgIC5zZXRDb2xvcignIzA4NDNjYycpXG4gICAgICAgICAgICApXG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBpbmZyYWN0aW9ucyA9IGF3YWl0IFByb21pc2UuYWxsKHdhcm5zLm1hcChhc3luYyAodjogV2FybnMsIGk6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgY29uc3QgbW9kOiBVc2VyID0gYXdhaXQgdGhpcy5jbGllbnQudXNlcnMuZmV0Y2godi5tb2RlcmF0b3IpLmNhdGNoKCgpID0+IG51bGwpO1xuICAgICAgICAgICAgaWYgKG1vZCkgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBpbmRleDogaSArIDEsXG4gICAgICAgICAgICAgICAgbW9kZXJhdG9yOiBtb2QudGFnLFxuICAgICAgICAgICAgICAgIHJlYXNvbjogdi5yZWFzb24sXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pKTtcblxuICAgICAgICByZXR1cm4gbWVzc2FnZS51dGlsLnNlbmQoXG4gICAgICAgICAgICBuZXcgTWVzc2FnZUVtYmVkKClcbiAgICAgICAgICAgIC5zZXRBdXRob3IoYEluZnJhY3Rpb25zIHwgJHttZW1iZXIudXNlci51c2VybmFtZX1gLCBtZW1iZXIudXNlci5kaXNwbGF5QXZhdGFyVVJMKHsgZHluYW1pYzogdHJ1ZSB9KSlcbiAgICAgICAgICAgIC5zZXRDb2xvcignIzA4NDNjYycpXG4gICAgICAgICAgICAuc2V0RGVzY3JpcHRpb24oaW5mcmFjdGlvbnMubWFwKHYgPT4gYFxcYCMke3YuaW5kZXh9XFxgIHwgTW9kZXJhdG9yOiBfXyR7di5tb2RlcmF0b3J9X19cXG5SZWFzb246ICoke3YucmVhc29ufSpcXG5gKSlcbiAgICAgICAgKVxuICAgIH1cbn0iXX0=