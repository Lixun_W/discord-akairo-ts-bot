"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
class DoCommand extends discord_akairo_1.Command {
    constructor() {
        super("do", {
            aliases: ['do', 'action', 'me'],
            category: 'Roleplay Commands',
            description: {
                content: 'Show an in game action',
                usage: 'Do <Action>',
                examples: [
                    `Do rolls down window`
                ]
            },
            args: [
                {
                    id: 'Action',
                    type: 'string',
                    prompt: {
                        start: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please provide an action!`,
                        retry: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please provide an action!`
                    }
                }
            ]
        });
    }
    async exec(message, { Action }) {
        message.delete();
        return message.channel.send(`**[Action] - ${message.author}** | ${Action}`);
    }
}
exports.default = DoCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRG9Db21tYW5kLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL1JvbGVwbGF5IENvbW1hbmRzL0RvQ29tbWFuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG1EQUF5QztBQUd6QyxNQUFxQixTQUFVLFNBQVEsd0JBQU87SUFFMUM7UUFDSSxLQUFLLENBQUMsSUFBSSxFQUFFO1lBQ1IsT0FBTyxFQUFFLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUM7WUFDL0IsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixXQUFXLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLHdCQUF3QjtnQkFDakMsS0FBSyxFQUFFLGFBQWE7Z0JBQ3BCLFFBQVEsRUFBRTtvQkFDTixzQkFBc0I7aUJBQ3pCO2FBQ0o7WUFDRCxJQUFJLEVBQUU7Z0JBQ0Y7b0JBQ0ksRUFBRSxFQUFFLFFBQVE7b0JBQ1osSUFBSSxFQUFFLFFBQVE7b0JBQ2QsTUFBTSxFQUFFO3dCQUNKLEtBQUssRUFBRSxDQUFDLEdBQVksRUFBRSxFQUFFLENBQUMsK0VBQStFO3dCQUN4RyxLQUFLLEVBQUUsQ0FBQyxHQUFZLEVBQUUsRUFBRSxDQUFDLCtFQUErRTtxQkFDM0c7aUJBQ0o7YUFDSjtTQUNKLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTSxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQWdCLEVBQUUsRUFBRSxNQUFNLEVBQUU7UUFFMUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFBO1FBRWhCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLE9BQU8sQ0FBQyxNQUFNLFFBQVEsTUFBTSxFQUFFLENBQUMsQ0FBQTtJQUMvRSxDQUFDO0NBQ0o7QUFoQ0QsNEJBZ0NDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbWFuZCB9IGZyb20gXCJkaXNjb3JkLWFrYWlyb1wiO1xuaW1wb3J0IHsgTWVzc2FnZSwgTWVzc2FnZUVtYmVkIH0gZnJvbSBcImRpc2NvcmQuanNcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRG9Db21tYW5kIGV4dGVuZHMgQ29tbWFuZCB7XG5cbiAgICBwdWJsaWMgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKFwiZG9cIiwge1xuICAgICAgICAgICAgYWxpYXNlczogWydkbycsICdhY3Rpb24nLCAnbWUnXSxcbiAgICAgICAgICAgIGNhdGVnb3J5OiAnUm9sZXBsYXkgQ29tbWFuZHMnLFxuICAgICAgICAgICAgZGVzY3JpcHRpb246IHtcbiAgICAgICAgICAgICAgICBjb250ZW50OiAnU2hvdyBhbiBpbiBnYW1lIGFjdGlvbicsXG4gICAgICAgICAgICAgICAgdXNhZ2U6ICdEbyA8QWN0aW9uPicsXG4gICAgICAgICAgICAgICAgZXhhbXBsZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgYERvIHJvbGxzIGRvd24gd2luZG93YFxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhcmdzOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDogJ0FjdGlvbicsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgICAgICAgICAgICBwcm9tcHQ6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0OiAobXNnOiBNZXNzYWdlKSA9PiBgPDpjcm9zc3Rpbmd5Ojc5ODMwOTI0Nzc1NzcxMzQwOD4gSW5jb3JyZWN0IHVzYWdlIVxcbiBQbGVhc2UgcHJvdmlkZSBhbiBhY3Rpb24hYCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHJ5OiAobXNnOiBNZXNzYWdlKSA9PiBgPDpjcm9zc3Rpbmd5Ojc5ODMwOTI0Nzc1NzcxMzQwOD4gSW5jb3JyZWN0IHVzYWdlIVxcbiBQbGVhc2UgcHJvdmlkZSBhbiBhY3Rpb24hYFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyBleGVjKG1lc3NhZ2U6IE1lc3NhZ2UsIHsgQWN0aW9uIH0pOiBQcm9taXNlPE1lc3NhZ2U+IHtcblxuICAgICAgICBtZXNzYWdlLmRlbGV0ZSgpXG5cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2UuY2hhbm5lbC5zZW5kKGAqKltBY3Rpb25dIC0gJHttZXNzYWdlLmF1dGhvcn0qKiB8ICR7QWN0aW9ufWApXG4gICAgfVxufSJdfQ==