"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
class OocCommand extends discord_akairo_1.Command {
    constructor() {
        super("ooc", {
            aliases: ['ooc'],
            category: 'Roleplay Commands',
            description: {
                content: 'Show an in game action',
                usage: 'Ooc <Action>',
                examples: [
                    `Ooc is blind`
                ]
            },
            args: [
                {
                    id: 'Action',
                    type: 'string',
                    prompt: {
                        start: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please provide an action!`,
                        retry: (msg) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please provide an action!`
                    }
                }
            ]
        });
    }
    async exec(message, { Action }) {
        message.delete();
        return message.channel.send(`**[OOC] - ${message.author}** | ${Action}`);
    }
}
exports.default = OocCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT29jQ29tbWFuZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21tYW5kcy9Sb2xlcGxheSBDb21tYW5kcy9Pb2NDb21tYW5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbURBQXlDO0FBR3pDLE1BQXFCLFVBQVcsU0FBUSx3QkFBTztJQUUzQztRQUNJLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDVCxPQUFPLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDaEIsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixXQUFXLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLHdCQUF3QjtnQkFDakMsS0FBSyxFQUFFLGNBQWM7Z0JBQ3JCLFFBQVEsRUFBRTtvQkFDTixjQUFjO2lCQUNqQjthQUNKO1lBQ0QsSUFBSSxFQUFFO2dCQUNGO29CQUNJLEVBQUUsRUFBRSxRQUFRO29CQUNaLElBQUksRUFBRSxRQUFRO29CQUNkLE1BQU0sRUFBRTt3QkFDSixLQUFLLEVBQUUsQ0FBQyxHQUFZLEVBQUUsRUFBRSxDQUFDLCtFQUErRTt3QkFDeEcsS0FBSyxFQUFFLENBQUMsR0FBWSxFQUFFLEVBQUUsQ0FBQywrRUFBK0U7cUJBQzNHO2lCQUNKO2FBQ0o7U0FDSixDQUFDLENBQUE7SUFDTixDQUFDO0lBRU0sS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFnQixFQUFFLEVBQUUsTUFBTSxFQUFFO1FBRTFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQTtRQUVoQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsT0FBTyxDQUFDLE1BQU0sUUFBUSxNQUFNLEVBQUUsQ0FBQyxDQUFBO0lBQzVFLENBQUM7Q0FDSjtBQWhDRCw2QkFnQ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tYW5kIH0gZnJvbSBcImRpc2NvcmQtYWthaXJvXCI7XG5pbXBvcnQgeyBNZXNzYWdlLCBNZXNzYWdlRW1iZWQgfSBmcm9tIFwiZGlzY29yZC5qc1wiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBPb2NDb21tYW5kIGV4dGVuZHMgQ29tbWFuZCB7XG5cbiAgICBwdWJsaWMgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKFwib29jXCIsIHtcbiAgICAgICAgICAgIGFsaWFzZXM6IFsnb29jJ10sXG4gICAgICAgICAgICBjYXRlZ29yeTogJ1JvbGVwbGF5IENvbW1hbmRzJyxcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiB7XG4gICAgICAgICAgICAgICAgY29udGVudDogJ1Nob3cgYW4gaW4gZ2FtZSBhY3Rpb24nLFxuICAgICAgICAgICAgICAgIHVzYWdlOiAnT29jIDxBY3Rpb24+JyxcbiAgICAgICAgICAgICAgICBleGFtcGxlczogW1xuICAgICAgICAgICAgICAgICAgICBgT29jIGlzIGJsaW5kYFxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhcmdzOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDogJ0FjdGlvbicsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgICAgICAgICAgICBwcm9tcHQ6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0OiAobXNnOiBNZXNzYWdlKSA9PiBgPDpjcm9zc3Rpbmd5Ojc5ODMwOTI0Nzc1NzcxMzQwOD4gSW5jb3JyZWN0IHVzYWdlIVxcbiBQbGVhc2UgcHJvdmlkZSBhbiBhY3Rpb24hYCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHJ5OiAobXNnOiBNZXNzYWdlKSA9PiBgPDpjcm9zc3Rpbmd5Ojc5ODMwOTI0Nzc1NzcxMzQwOD4gSW5jb3JyZWN0IHVzYWdlIVxcbiBQbGVhc2UgcHJvdmlkZSBhbiBhY3Rpb24hYFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyBleGVjKG1lc3NhZ2U6IE1lc3NhZ2UsIHsgQWN0aW9uIH0pOiBQcm9taXNlPE1lc3NhZ2U+IHtcblxuICAgICAgICBtZXNzYWdlLmRlbGV0ZSgpXG5cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2UuY2hhbm5lbC5zZW5kKGAqKltPT0NdIC0gJHttZXNzYWdlLmF1dGhvcn0qKiB8ICR7QWN0aW9ufWApXG4gICAgfVxufSJdfQ==