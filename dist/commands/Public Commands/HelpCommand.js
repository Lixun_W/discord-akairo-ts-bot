"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
const common_tags_1 = require("common-tags");
class HelpCommand extends discord_akairo_1.Command {
    constructor() {
        super("help", {
            aliases: ['help', 'commands', 'cmds'],
            category: 'Public Commands',
            description: {
                content: "View the available commands for the bot",
                usage: "help [Command]",
                examples: [
                    "help",
                    "help warn"
                ]
            },
            ratelimit: 3,
            args: [
                {
                    id: "command",
                    type: "commandAlias",
                    default: null
                }
            ]
        });
    }
    exec(message, { command }) {
        if (command) {
            return message.channel.send(new discord_js_1.MessageEmbed()
                .setAuthor(`Help | ${command}`, this.client.user.displayAvatarURL())
                .setColor('#0843cc')
                .setDescription(common_tags_1.stripIndents `
                    **Description:**
                    ${command.description.content || "No description available"}

                    **Usage:**
                    ${command.description.usage || "No usage available"}

                    **Examples:**
                    ${command.description.examples ? command.description.examples.map(e => `\`${e}\``).join("\n") : "No examples available"}
                `));
        }
        const embed = new discord_js_1.MessageEmbed()
            .setAuthor(`Help | ${this.client.user.username}`, this.client.user.displayAvatarURL())
            .setColor('#0843cc')
            .setFooter(`${this.client.commandHandler.prefix}help [Command] For more information on a command`);
        for (const category of this.handler.categories.values()) {
            if (["default"].includes(category.id))
                continue;
            embed.addField(category.id, category
                .filter(cmd => cmd.aliases.length > 0)
                .map(cmd => `\`${cmd}\``)
                .join(' | ') || "No commands in this category");
        }
        return message.channel.send(embed);
    }
}
exports.default = HelpCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSGVscENvbW1hbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29tbWFuZHMvUHVibGljIENvbW1hbmRzL0hlbHBDb21tYW5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbURBQXlDO0FBQ3pDLDJDQUFtRDtBQUNuRCw2Q0FBMkM7QUFFM0MsTUFBcUIsV0FBWSxTQUFRLHdCQUFPO0lBQzVDO1FBQ0ksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNWLE9BQU8sRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDO1lBQ3JDLFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsV0FBVyxFQUFFO2dCQUNULE9BQU8sRUFBRSx5Q0FBeUM7Z0JBQ2xELEtBQUssRUFBRSxnQkFBZ0I7Z0JBQ3ZCLFFBQVEsRUFBRTtvQkFDTixNQUFNO29CQUNOLFdBQVc7aUJBQ2Q7YUFDSjtZQUNELFNBQVMsRUFBRSxDQUFDO1lBQ1osSUFBSSxFQUFFO2dCQUNGO29CQUNJLEVBQUUsRUFBRSxTQUFTO29CQUNiLElBQUksRUFBRSxjQUFjO29CQUNwQixPQUFPLEVBQUUsSUFBSTtpQkFDaEI7YUFDSjtTQUNKLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTSxJQUFJLENBQUMsT0FBZ0IsRUFBRSxFQUFFLE9BQU8sRUFBd0I7UUFDM0QsSUFBSSxPQUFPLEVBQUU7WUFDVCxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUN2QixJQUFJLHlCQUFZLEVBQUU7aUJBQ2pCLFNBQVMsQ0FBQyxVQUFVLE9BQU8sRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7aUJBQ25FLFFBQVEsQ0FBQyxTQUFTLENBQUM7aUJBQ25CLGNBQWMsQ0FBQywwQkFBWSxDQUFBOztzQkFFdEIsT0FBTyxDQUFDLFdBQVcsQ0FBQyxPQUFPLElBQUksMEJBQTBCOzs7c0JBR3pELE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxJQUFJLG9CQUFvQjs7O3NCQUdqRCxPQUFPLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsdUJBQXVCO2lCQUMxSCxDQUFDLENBQ0wsQ0FBQTtTQUNKO1FBRUQsTUFBTSxLQUFLLEdBQUcsSUFBSSx5QkFBWSxFQUFFO2FBQzNCLFNBQVMsQ0FBQyxVQUFVLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7YUFDckYsUUFBUSxDQUFDLFNBQVMsQ0FBQzthQUNuQixTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLGtEQUFrRCxDQUFDLENBQUE7UUFFdEcsS0FBSyxNQUFNLFFBQVEsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUNyRCxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUM7Z0JBQUUsU0FBUztZQUVoRCxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsUUFBUTtpQkFDL0IsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2lCQUNyQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2lCQUN4QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksOEJBQThCLENBQ2pELENBQUE7U0FDSjtRQUVELE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7SUFDdEMsQ0FBQztDQUNKO0FBNURELDhCQTREQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1hbmQgfSBmcm9tIFwiZGlzY29yZC1ha2Fpcm9cIjtcbmltcG9ydCB7IE1lc3NhZ2UsIE1lc3NhZ2VFbWJlZCB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XG5pbXBvcnQgeyBzdHJpcEluZGVudHMgfSBmcm9tIFwiY29tbW9uLXRhZ3NcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSGVscENvbW1hbmQgZXh0ZW5kcyBDb21tYW5kIHtcbiAgICBwdWJsaWMgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKFwiaGVscFwiLCB7XG4gICAgICAgICAgICBhbGlhc2VzOiBbJ2hlbHAnLCAnY29tbWFuZHMnLCAnY21kcyddLFxuICAgICAgICAgICAgY2F0ZWdvcnk6ICdQdWJsaWMgQ29tbWFuZHMnLFxuICAgICAgICAgICAgZGVzY3JpcHRpb246IHtcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIlZpZXcgdGhlIGF2YWlsYWJsZSBjb21tYW5kcyBmb3IgdGhlIGJvdFwiLFxuICAgICAgICAgICAgICAgIHVzYWdlOiBcImhlbHAgW0NvbW1hbmRdXCIsXG4gICAgICAgICAgICAgICAgZXhhbXBsZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgXCJoZWxwXCIsXG4gICAgICAgICAgICAgICAgICAgIFwiaGVscCB3YXJuXCJcbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgcmF0ZWxpbWl0OiAzLFxuICAgICAgICAgICAgYXJnczogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6IFwiY29tbWFuZFwiLFxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcImNvbW1hbmRBbGlhc1wiLFxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OiBudWxsXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIHB1YmxpYyBleGVjKG1lc3NhZ2U6IE1lc3NhZ2UsIHsgY29tbWFuZCB9OiB7IGNvbW1hbmQ6IENvbW1hbmQgfSk6IFByb21pc2U8TWVzc2FnZT4ge1xuICAgICAgICBpZiAoY29tbWFuZCkge1xuICAgICAgICAgICAgcmV0dXJuIG1lc3NhZ2UuY2hhbm5lbC5zZW5kKFxuICAgICAgICAgICAgICAgIG5ldyBNZXNzYWdlRW1iZWQoKVxuICAgICAgICAgICAgICAgIC5zZXRBdXRob3IoYEhlbHAgfCAke2NvbW1hbmR9YCwgdGhpcy5jbGllbnQudXNlci5kaXNwbGF5QXZhdGFyVVJMKCkpXG4gICAgICAgICAgICAgICAgLnNldENvbG9yKCcjMDg0M2NjJylcbiAgICAgICAgICAgICAgICAuc2V0RGVzY3JpcHRpb24oc3RyaXBJbmRlbnRzYFxuICAgICAgICAgICAgICAgICAgICAqKkRlc2NyaXB0aW9uOioqXG4gICAgICAgICAgICAgICAgICAgICR7Y29tbWFuZC5kZXNjcmlwdGlvbi5jb250ZW50IHx8IFwiTm8gZGVzY3JpcHRpb24gYXZhaWxhYmxlXCJ9XG5cbiAgICAgICAgICAgICAgICAgICAgKipVc2FnZToqKlxuICAgICAgICAgICAgICAgICAgICAke2NvbW1hbmQuZGVzY3JpcHRpb24udXNhZ2UgfHwgXCJObyB1c2FnZSBhdmFpbGFibGVcIn1cblxuICAgICAgICAgICAgICAgICAgICAqKkV4YW1wbGVzOioqXG4gICAgICAgICAgICAgICAgICAgICR7Y29tbWFuZC5kZXNjcmlwdGlvbi5leGFtcGxlcyA/IGNvbW1hbmQuZGVzY3JpcHRpb24uZXhhbXBsZXMubWFwKGUgPT4gYFxcYCR7ZX1cXGBgKS5qb2luKFwiXFxuXCIpIDogXCJObyBleGFtcGxlcyBhdmFpbGFibGVcIn1cbiAgICAgICAgICAgICAgICBgKVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgZW1iZWQgPSBuZXcgTWVzc2FnZUVtYmVkKClcbiAgICAgICAgICAgIC5zZXRBdXRob3IoYEhlbHAgfCAke3RoaXMuY2xpZW50LnVzZXIudXNlcm5hbWV9YCwgdGhpcy5jbGllbnQudXNlci5kaXNwbGF5QXZhdGFyVVJMKCkpXG4gICAgICAgICAgICAuc2V0Q29sb3IoJyMwODQzY2MnKVxuICAgICAgICAgICAgLnNldEZvb3RlcihgJHt0aGlzLmNsaWVudC5jb21tYW5kSGFuZGxlci5wcmVmaXh9aGVscCBbQ29tbWFuZF0gRm9yIG1vcmUgaW5mb3JtYXRpb24gb24gYSBjb21tYW5kYClcblxuICAgICAgICBmb3IgKGNvbnN0IGNhdGVnb3J5IG9mIHRoaXMuaGFuZGxlci5jYXRlZ29yaWVzLnZhbHVlcygpKSB7XG4gICAgICAgICAgICBpZiAoW1wiZGVmYXVsdFwiXS5pbmNsdWRlcyhjYXRlZ29yeS5pZCkpIGNvbnRpbnVlO1xuXG4gICAgICAgICAgICBlbWJlZC5hZGRGaWVsZChjYXRlZ29yeS5pZCwgY2F0ZWdvcnlcbiAgICAgICAgICAgICAgICAuZmlsdGVyKGNtZCA9PiBjbWQuYWxpYXNlcy5sZW5ndGggPiAwKVxuICAgICAgICAgICAgICAgIC5tYXAoY21kID0+IGBcXGAke2NtZH1cXGBgKVxuICAgICAgICAgICAgICAgIC5qb2luKCcgfCAnKSB8fCBcIk5vIGNvbW1hbmRzIGluIHRoaXMgY2F0ZWdvcnlcIlxuICAgICAgICAgICAgKVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2UuY2hhbm5lbC5zZW5kKGVtYmVkKVxuICAgIH1cbn0iXX0=