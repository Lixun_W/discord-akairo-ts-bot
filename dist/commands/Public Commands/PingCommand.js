"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
class PingCommand extends discord_akairo_1.Command {
    constructor() {
        super("ping", {
            aliases: ['ping', 'latency'],
            category: 'Public Commands',
            description: {
                content: 'Show the latency of the bot'
            },
            ratelimit: 3
        });
    }
    exec(message) {
        return message.channel.send(new discord_js_1.MessageEmbed()
            .setTitle(`Ping For ${this.client.user.username}`)
            .setDescription(`My Ping Is **${this.client.ws.ping}ms**`)
            .setColor('#0843cc')
            .setTimestamp());
    }
}
exports.default = PingCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGluZ0NvbW1hbmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29tbWFuZHMvUHVibGljIENvbW1hbmRzL1BpbmdDb21tYW5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbURBQXlDO0FBQ3pDLDJDQUFtRDtBQUVuRCxNQUFxQixXQUFZLFNBQVEsd0JBQU87SUFFNUM7UUFDSSxLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ1YsT0FBTyxFQUFFLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQztZQUM1QixRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLFdBQVcsRUFBRTtnQkFDVCxPQUFPLEVBQUUsNkJBQTZCO2FBQ3pDO1lBQ0QsU0FBUyxFQUFFLENBQUM7U0FDZixDQUFDLENBQUE7SUFDTixDQUFDO0lBRU0sSUFBSSxDQUFDLE9BQWdCO1FBRXhCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQ3ZCLElBQUkseUJBQVksRUFBRTthQUNqQixRQUFRLENBQUMsWUFBWSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNqRCxjQUFjLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksTUFBTSxDQUFDO2FBQ3pELFFBQVEsQ0FBQyxTQUFTLENBQUM7YUFDbkIsWUFBWSxFQUFFLENBQ2xCLENBQUE7SUFDTCxDQUFDO0NBQ0o7QUF2QkQsOEJBdUJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbWFuZCB9IGZyb20gXCJkaXNjb3JkLWFrYWlyb1wiO1xuaW1wb3J0IHsgTWVzc2FnZSwgTWVzc2FnZUVtYmVkIH0gZnJvbSBcImRpc2NvcmQuanNcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGluZ0NvbW1hbmQgZXh0ZW5kcyBDb21tYW5kIHtcblxuICAgIHB1YmxpYyBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoXCJwaW5nXCIsIHtcbiAgICAgICAgICAgIGFsaWFzZXM6IFsncGluZycsICdsYXRlbmN5J10sXG4gICAgICAgICAgICBjYXRlZ29yeTogJ1B1YmxpYyBDb21tYW5kcycsXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjoge1xuICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICdTaG93IHRoZSBsYXRlbmN5IG9mIHRoZSBib3QnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgcmF0ZWxpbWl0OiAzXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgcHVibGljIGV4ZWMobWVzc2FnZTogTWVzc2FnZSk6IFByb21pc2U8TWVzc2FnZT4ge1xuXG4gICAgICAgIHJldHVybiBtZXNzYWdlLmNoYW5uZWwuc2VuZChcbiAgICAgICAgICAgIG5ldyBNZXNzYWdlRW1iZWQoKVxuICAgICAgICAgICAgLnNldFRpdGxlKGBQaW5nIEZvciAke3RoaXMuY2xpZW50LnVzZXIudXNlcm5hbWV9YClcbiAgICAgICAgICAgIC5zZXREZXNjcmlwdGlvbihgTXkgUGluZyBJcyAqKiR7dGhpcy5jbGllbnQud3MucGluZ31tcyoqYClcbiAgICAgICAgICAgIC5zZXRDb2xvcignIzA4NDNjYycpXG4gICAgICAgICAgICAuc2V0VGltZXN0YW1wKClcbiAgICAgICAgKVxuICAgIH1cbn0iXX0=