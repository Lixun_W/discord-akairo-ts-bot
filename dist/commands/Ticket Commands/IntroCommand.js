"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
class IntroCommmand extends discord_akairo_1.Command {
    constructor() {
        super("intro", {
            aliases: ['intro'],
            category: 'Ticket Commands',
            description: {
                content: 'Send the intro embed'
            },
            ratelimit: 3
        });
    }
    async exec(message) {
        message.delete();
        return message.channel.send(new discord_js_1.MessageEmbed()
            .setAuthor(message.guild.name)
            .setDescription(`Hello, my name is ${message.author} with ${message.guild.name}. Thank you, for choosing us on this lovely day today.

            How may I assist you?`)
            .setFooter(`Sent by: ${message.author.id}`, message.author.displayAvatarURL({ dynamic: true }))
            .setTimestamp()
            .setThumbnail(message.guild.iconURL({ dynamic: true }))
            .setColor("LIGHT_GREY"));
    }
}
exports.default = IntroCommmand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW50cm9Db21tYW5kLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL1RpY2tldCBDb21tYW5kcy9JbnRyb0NvbW1hbmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxtREFBeUM7QUFDekMsMkNBQW1EO0FBRW5ELE1BQXFCLGFBQWMsU0FBUSx3QkFBTztJQUU5QztRQUNJLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDWCxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUM7WUFDbEIsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixXQUFXLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLHNCQUFzQjthQUNsQztZQUNELFNBQVMsRUFBRSxDQUFDO1NBQ2YsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVNLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBZ0I7UUFFOUIsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFBO1FBRWhCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQ3ZCLElBQUkseUJBQVksRUFBRTthQUNqQixTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7YUFDN0IsY0FBYyxDQUFDLHFCQUFxQixPQUFPLENBQUMsTUFBTSxTQUFTLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSTs7a0NBRXhELENBQUM7YUFDdEIsU0FBUyxDQUFDLFlBQVksT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7YUFDOUYsWUFBWSxFQUFFO2FBQ2QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7YUFDdEQsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUMxQixDQUFBO0lBQ0wsQ0FBQztDQUNKO0FBN0JELGdDQTZCQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1hbmQgfSBmcm9tIFwiZGlzY29yZC1ha2Fpcm9cIjtcbmltcG9ydCB7IE1lc3NhZ2UsIE1lc3NhZ2VFbWJlZCB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEludHJvQ29tbW1hbmQgZXh0ZW5kcyBDb21tYW5kIHtcblxuICAgIHB1YmxpYyBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoXCJpbnRyb1wiLCB7XG4gICAgICAgICAgICBhbGlhc2VzOiBbJ2ludHJvJ10sXG4gICAgICAgICAgICBjYXRlZ29yeTogJ1RpY2tldCBDb21tYW5kcycsXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjoge1xuICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICdTZW5kIHRoZSBpbnRybyBlbWJlZCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICByYXRlbGltaXQ6IDNcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBwdWJsaWMgYXN5bmMgZXhlYyhtZXNzYWdlOiBNZXNzYWdlKTogUHJvbWlzZTxNZXNzYWdlPiB7XG5cbiAgICAgICAgbWVzc2FnZS5kZWxldGUoKVxuXG4gICAgICAgIHJldHVybiBtZXNzYWdlLmNoYW5uZWwuc2VuZChcbiAgICAgICAgICAgIG5ldyBNZXNzYWdlRW1iZWQoKVxuICAgICAgICAgICAgLnNldEF1dGhvcihtZXNzYWdlLmd1aWxkLm5hbWUpXG4gICAgICAgICAgICAuc2V0RGVzY3JpcHRpb24oYEhlbGxvLCBteSBuYW1lIGlzICR7bWVzc2FnZS5hdXRob3J9IHdpdGggJHttZXNzYWdlLmd1aWxkLm5hbWV9LiBUaGFuayB5b3UsIGZvciBjaG9vc2luZyB1cyBvbiB0aGlzIGxvdmVseSBkYXkgdG9kYXkuXG5cbiAgICAgICAgICAgIEhvdyBtYXkgSSBhc3Npc3QgeW91P2ApXG4gICAgICAgICAgICAuc2V0Rm9vdGVyKGBTZW50IGJ5OiAke21lc3NhZ2UuYXV0aG9yLmlkfWAsIG1lc3NhZ2UuYXV0aG9yLmRpc3BsYXlBdmF0YXJVUkwoeyBkeW5hbWljOiB0cnVlIH0pKVxuICAgICAgICAgICAgLnNldFRpbWVzdGFtcCgpXG4gICAgICAgICAgICAuc2V0VGh1bWJuYWlsKG1lc3NhZ2UuZ3VpbGQuaWNvblVSTCh7IGR5bmFtaWM6IHRydWUgfSkpXG4gICAgICAgICAgICAuc2V0Q29sb3IoXCJMSUdIVF9HUkVZXCIpXG4gICAgICAgIClcbiAgICB9XG59Il19