import { ConnectionManager } from "typeorm";
import { Warns } from "../Models/Warns";
import { dbName } from "../config";
import { connect } from "http2";

const connectionManager: ConnectionManager = new ConnectionManager();
connectionManager.create({
    name: dbName,
    type: "sqlite",
    database: "./db.sqlite",
    entities: [
        Warns,
    ]
});

export default connectionManager;