import { Command } from "discord-akairo";
import { GuildMember } from "discord.js";
import { Message, MessageEmbed } from "discord.js";

export default class KickCommand extends Command {

    public constructor() {
        super("kick", {
            aliases: ['kick', 'k'],
            category: "Moderation Commands",
            description: {
                content: 'Kick someone from the server',
                usage: 'kick <Member> [Reason]',
                examples: [
                    'kick @Lukas',
                    'kick Lukas W'
                ]
            },
            userPermissions: ["KICK_MEMBERS"],
            args: [
                {
                    id: "member",
                    type: "member",
                    prompt: {
                        start: (msg: Message) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to kick them!`,
                        retry: (msg: Message) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to kick them!`
                    }
                },
                {
                    id: "reason",
                    type: "string",
                    match: "rest",
                    default: "No Reason Provided"
                }
            ]
        })
    }

    public async exec(message: Message, { member, reason }: { member: GuildMember, reason: string }): Promise<Message> {

        if (member.roles.highest.position >= message.member.roles.highest.position && message.author.id !== message.guild.ownerID)
        return message.util.send(
            new MessageEmbed()
            .setTitle(`Error`)
            .setDescription(`This member is higher than you therefore a kick cannot be given`)
            .setTimestamp()
            .setColor('#0843cc')
        )

        member.kick()

        return message.util.send(
            new MessageEmbed()
            .setTitle(`Kicked!`)
            .setDescription(`Kicked: ${member.user.tag}, Reason: ${reason}`)
            .setTimestamp()
            .setColor('#0843cc')
        )
    }
}