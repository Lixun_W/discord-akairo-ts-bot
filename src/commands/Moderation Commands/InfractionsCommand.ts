import { Command } from "discord-akairo";
import { Message, GuildMember, User, MessageEmbed } from "discord.js";
import { Repository } from "typeorm";
import { Warns } from "../../Models/Warns";

export default class InfractionsCommand extends Command {
    public constructor() {
        super("infractions", {
            aliases: ['punishments', 'warns'],
            category: "Moderation Commands",
            description: {
                content: "Check infractions of a member",
                usage: "infractions <Member>",
                examples: [
                    "infractions @Lukas",
                    "infractions lukas"
                ]
            },
            args: [
                {
                    id: "member",
                    type: "member",
                    default: (msg: Message) => msg.member
                }
            ]
        })
    }

    public async exec(message: Message, { member }: { member: GuildMember }): Promise<Message> {
        const warnRepo: Repository<Warns> = this.client.db.getRepository(Warns);

        const warns: Warns[] = await warnRepo.find({ user: member.id, guild: message.guild.id });

        if (!warns.length) {
            return message.util.send(
                new MessageEmbed()
                .setTitle(`Congrats!`)
                .setDescription(`${member.user.tag} does not have any warns inside my database`)
                .setTimestamp()
                .setColor('#0843cc')
            )
        }

        const infractions = await Promise.all(warns.map(async (v: Warns, i: number) => {
            const mod: User = await this.client.users.fetch(v.moderator).catch(() => null);
            if (mod) return {
                index: i + 1,
                moderator: mod.tag,
                reason: v.reason,
            }
        }));

        return message.util.send(
            new MessageEmbed()
            .setAuthor(`Infractions | ${member.user.username}`, member.user.displayAvatarURL({ dynamic: true }))
            .setColor('#0843cc')
            .setDescription(infractions.map(v => `\`#${v.index}\` | Moderator: __${v.moderator}__\nReason: *${v.reason}*\n`))
        )
    }
}