import { Command } from "discord-akairo";
import { Guild } from "discord.js";
import { Message, GuildMember, MessageEmbed } from "discord.js";
import { Repository } from "typeorm";
import { Warns } from "../../Models/Warns";

export default class WarnCommand extends Command {
    public constructor() {
        super("warn", {
            aliases: ["w"],
            category: "Moderation Commands",
            description: {
                content: "Warn a member for something they have done",
                usage: "warn <Member> <Reason>",
                examples: [
                    "warn @Lukas Being bad",,
                    "warn lukas being bad"
                ]
            },
            userPermissions: ["MANAGE_MESSAGES"],
            args: [
                {
                    id: "member",
                    type: "member",
                    prompt: {
                        start: (msg: Message) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to warn them!`,
                        retry: (msg: Message) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to warn them!`
                    }
                },
                {
                    id: "reason",
                    type: "string",
                    match: "rest",
                    default: "No Reason Provided"
                }
            ]
        });
    }

    public async exec(message: Message, { member, reason }: { member: GuildMember, reason: string }): Promise<Message> {

        const warnRepo: Repository<Warns> = this.client.db.getRepository(Warns);

        if (member.roles.highest.position >= message.member.roles.highest.position && message.author.id !== message.guild.ownerID)
        return message.util.send(
            new MessageEmbed()
            .setTitle(`Error`)
            .setDescription(`This member is higher than you therefore a warn cannot be given`)
            .setTimestamp()
            .setColor('#0843cc')
        )
        
        await warnRepo.insert({
            guild: message.guild.id,
            user: member.id,
            moderator: message.author.id,
            reason: reason
        });

        return message.util.send(
            new MessageEmbed()
            .setTitle(`Warned`)
            .setDescription(`${member.user.tag} Has Been Warned By ${message.author.tag} For ${reason}`)
            .setColor('#0843cc')
        )
    }
}