import { Command } from "discord-akairo";
import { GuildMember } from "discord.js";
import { Message, MessageEmbed } from "discord.js";

export default class BanCommand extends Command {

    public constructor() {
        super("ban", {
            aliases: ['ban', 'b'],
            category: "Moderation Commands",
            description: {
                content: 'Ban someone from the server',
                usage: 'ban <Member> [Reason]',
                examples: [
                    'ban @Lukas',
                    'ban Lukas W'
                ]
            },
            userPermissions: ["BAN_MEMBERS"],
            args: [
                {
                    id: "member",
                    type: "member",
                    prompt: {
                        start: (msg: Message) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to ban them!`,
                        retry: (msg: Message) => `<:crosstingy:798309247757713408> Incorrect usage!\n Please mention someone to ban them!`
                    }
                },
                {
                    id: "reason",
                    type: "string",
                    match: "rest",
                    default: "No Reason Provided"
                }
            ]
        })
    }

    public async exec(message: Message, { member, reason }: { member: GuildMember, reason: string }): Promise<Message> {

        if (member.roles.highest.position >= message.member.roles.highest.position && message.author.id !== message.guild.ownerID)
        return message.util.send(
            new MessageEmbed()
            .setTitle(`Error`)
            .setDescription(`This member is higher than you therefore a ban cannot be given`)
            .setTimestamp()
            .setColor('#0843cc')
        )

        member.ban()

        return message.util.send(
            new MessageEmbed()
            .setTitle(`Banned!`)
            .setDescription(`Banned: ${member.user.tag}, Reason: ${reason}`)
            .setTimestamp()
            .setColor('#0843cc')
        )
    }
}