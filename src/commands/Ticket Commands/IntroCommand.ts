import { Command } from "discord-akairo";
import { Message, MessageEmbed } from "discord.js";

export default class IntroCommmand extends Command {

    public constructor() {
        super("intro", {
            aliases: ['intro'],
            category: 'Ticket Commands',
            description: {
                content: 'Send the intro embed'
            },
            ratelimit: 3
        })
    }

    public async exec(message: Message): Promise<Message> {

        message.delete()

        return message.channel.send(
            new MessageEmbed()
            .setAuthor(message.guild.name)
            .setDescription(`Hello, my name is ${message.author} with ${message.guild.name}. Thank you, for choosing us on this lovely day today.

            How may I assist you?`)
            .setFooter(`Sent by: ${message.author.id}`, message.author.displayAvatarURL({ dynamic: true }))
            .setTimestamp()
            .setThumbnail(message.guild.iconURL({ dynamic: true }))
            .setColor("LIGHT_GREY")
        )
    }
}