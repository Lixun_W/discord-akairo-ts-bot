import { Listener } from "discord-akairo";
import { Message, MessageEmbed, TextChannel } from "discord.js";

export default class ChannelDeleteListener extends Listener {

    public constructor() {
        super("channelDelete", {
            event: "channelDelete",
            emitter: "client",
            category: "client"
        })
    }

    public exec(channel: TextChannel): Promise<Message> {

        const LoggingChannel: TextChannel = this.client.channels.cache.get("831299355943632968") as TextChannel;

        return LoggingChannel.send(
            new MessageEmbed()
            .setTitle(`Channel Deleted`)
            .setDescription(`Channel Deleted: ${channel.name}`)
            .setColor('#0843cc')
        )
    }
}