import { Listener } from "discord-akairo";
import { Message, MessageEmbed, TextChannel } from "discord.js";

export default class MessageDeleteListener extends Listener {
    public constructor() {
        super("messageDelete", {
            event: "messageDelete",
            emitter: "client",
            category: "client"
        })
    }

    public async exec(message: Message): Promise<Message> {

        if (message.author.bot) return

        const channel: TextChannel = this.client.channels.cache.get("817464402018041906") as TextChannel;

        return channel.send(
            new MessageEmbed()
            .setAuthor(`Message Deleted | ${message.author.tag}`, message.author.displayAvatarURL({ dynamic: true }))
            .setDescription(message.content)
            .setColor('#0843cc')
            .addField(`Author`, `${message.author} (\`${message.author.id}\`)`, true)
            .addField(`Channel:`, `${message.channel} (\`${message.channel.id}\`)`, true)
            .setThumbnail(message.author.displayAvatarURL({ dynamic: true }))
        )
    }
}