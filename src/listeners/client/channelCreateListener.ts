import { Listener } from "discord-akairo";
import { Message, MessageEmbed, TextChannel } from "discord.js";

export default class ChannelCreateListener extends Listener {

    public constructor() {
        super("channelCreate", {
            event: "channelCreate",
            emitter: "client",
            category: "client"
        })
    }

    public async exec(channel: TextChannel): Promise<Message> {

        const LoggingChannel: TextChannel = this.client.channels.cache.get("831319698930532392") as TextChannel;

        return LoggingChannel.send(
            new MessageEmbed()
            .setTitle(`Channel Created`)
            .setDescription(`Channel Created: ${channel.name}`)
            .setColor('#0843cc')
        )
    }
}