import { Listener } from "discord-akairo";
import { Message, MessageEmbed, TextChannel } from "discord.js";

export default class MessageUpdateListener extends Listener {
    public constructor() {
        super("messageUpdate", {
            event: "messageUpdate",
            emitter: "client",
            category: "client"
        })
    }

    public async exec(oldMessage: Message, newMessage: Message): Promise<Message> {

        if (oldMessage.author.bot) return

        const channel: TextChannel = this.client.channels.cache.get("831297031154696212") as TextChannel;
        
        return channel.send(
            new MessageEmbed()
            .setAuthor(`Message Updated | ${oldMessage.author.tag}`, oldMessage.author.displayAvatarURL({ dynamic: true }))
            .setColor('#0843cc')
            .addField(`Old Content`, oldMessage, true)
            .addField(`New Message`, newMessage, true)
            .addField(`Author`, `${oldMessage.author} (\`${oldMessage.author.id}\`)`)
            .addField(`Channel:`, `${oldMessage.channel} (\`${oldMessage.channel.id}\`)`, true)
            .setThumbnail(oldMessage.author.displayAvatarURL({ dynamic: true }))
        )
    }
}